/*
 * Copyright (c) 2011 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.rfx.core.configs;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import rfx.core.util.CommonUtil;
import rfx.core.util.io.FileUtils;


/**
 * @author dungth5
 * 
 */
public class DataStoreConfigs {
  static DataStoreConfigs _instance;

  // Propertices
  public String path;

  public static DataStoreConfigs load() {
    if (_instance == null) {
      try {
        String json = FileUtils.readFileAsString(CommonUtil.getDataStoreConfigFile());
        _instance = new Gson().fromJson(json, DataStoreConfigs.class);
      } catch (Exception e) {
        if (e instanceof JsonSyntaxException) {
          e.printStackTrace();
          System.err
              .println("Wrong JSON syntax in file " + CommonUtil.getDataStoreConfigFile());
        } else {
          e.printStackTrace();
        }

      }
    }
    return _instance;
  }
}
