/*
 * Copyright (c) 2011 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.google.api.services.samples.analytics.cmdline;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.Analytics.Data.Ga.Get;
import com.google.api.services.analytics.AnalyticsScopes;
import com.google.api.services.analytics.model.Accounts;
import com.google.api.services.analytics.model.GaData;
import com.google.api.services.analytics.model.Profiles;
import com.google.api.services.analytics.model.Webproperties;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;

/**
 * @author dungth5@google.com (Your Name Here)
 * 
 */
public class ServiceAccountApiReferenceSample {
  private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
  private static final JsonFactory JSON_FACTORY = new JacksonFactory();

  public static void main(String[] args) throws GeneralSecurityException, IOException {
    analyticsExample();
  }

  public static void analyticsExample() throws GeneralSecurityException, IOException {

    // This is the .p12 file you got from the google api console by clicking generate new key
    File analyticsKeyFile =
        new File("/home/dungth5/Downloads/0ce3025f148e36d0077642163e80e73eb2e83c7e-privatekey.p12");

    // This is the service account email address that you can find in the api console
    String apiEmail = "k6kqp5lqtscc22onmh099h8hvb3ga215@developer.gserviceaccount.com";

    Credential credential =
        new GoogleCredential.Builder().setTransport(HTTP_TRANSPORT).setJsonFactory(JSON_FACTORY)
            .setServiceAccountId(apiEmail)
            .setServiceAccountScopes(Arrays.asList(AnalyticsScopes.ANALYTICS_READONLY))
            .setServiceAccountPrivateKeyFromP12File(analyticsKeyFile).build();

    Analytics analyticsService =
        new Analytics.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(
            "Service Account Analytics").build();

    GaData gaData = executeDataQuery(analyticsService, "ga:40525586");
  }
  
  private static GaData executeDataQuery(Analytics analytics, String tableId) throws IOException {
    return analytics.data().ga().get(tableId, // Table Id.
        "2014-06-12", // Start date.
        "2014-06-12", // End date.
        "ga:pageviews,ga:visits,ga:users") // Metrics.
        .setDimensions("ga:pagePath")
        .setSort("-ga:pageviews,ga:visits,ga:users")
//        .setFilters("ga:medium==organic")
        .setMaxResults(100)
        .execute();
  }

  private static String getFirstProfileId(Analytics analytics) throws IOException {
    String profileId = null;

    // Query accounts collection.
    Accounts accounts = analytics.management().accounts().list().execute();

    if (accounts.getItems().isEmpty()) {
      System.err.println("No accounts found");
    } else {
      String firstAccountId = accounts.getItems().get(0).getId();

      // Query webproperties collection.
      Webproperties webproperties =
          analytics.management().webproperties().list(firstAccountId).execute();

      if (webproperties.getItems().isEmpty()) {
        System.err.println("No Webproperties found");
      } else {
        String firstWebpropertyId = webproperties.getItems().get(0).getId();

        // Query profiles collection.
        Profiles profiles =
            analytics.management().profiles().list(firstAccountId, firstWebpropertyId).execute();

        if (profiles.getItems().isEmpty()) {
          System.err.println("No profiles found");
        } else {
          profileId = profiles.getItems().get(0).getId();
        }
      }
    }
    return profileId;
  }
}
