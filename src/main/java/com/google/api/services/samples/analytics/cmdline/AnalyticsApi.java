/*
 * Copyright (c) 2011 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.google.api.services.samples.analytics.cmdline;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.DataStoreFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.AnalyticsScopes;
import com.google.api.services.analytics.model.GaData;
import com.google.api.services.analytics.model.GaData.ColumnHeaders;
import com.google.api.services.analytics.model.GaData.ProfileInfo;
import com.google.api.services.analytics.model.GaData.Query;
import com.rfx.core.configs.DataStoreConfigs;

import rfx.core.model.GaDailyTech;
import rfx.core.model.GaDailyUser;
import rfx.core.util.StringUtil;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author dungth5
 * 
 */
public class AnalyticsApi {

  /**
   * Be sure to specify the name of your application. If the application name is {@code null} or
   * blank, the application will log a warning. Suggested format is "MyCompany-ProductName/1.0".
   */
  private static final String APPLICATION_NAME = "AnalyticsApi";

  /**
   * Used to identify from which reporting profile to retrieve data. Format is ga:xxx where xxx is
   * your profile ID.
   */
  // 0.0.1 VnExpress.net - Total - News/Web/mobile: 40525586
  // 0.0.2 VnExpress.net - News - Web: 40525857
  // 0.0.3 VnExpress.net - Root - Web: 362306
  // 0.0.4 VnExpress.net - Mobile: 22229355
  // 0.0.5 VnExpress.net - Beta / Reponsive: 82351572
  // 0.1.1 VnExpress.net - The thao - Web: 58329854
  // 0.1.2 VnExpress.net - The thao - Mobile: 61016540
  // 0.2.1 VnExpress.net - Giai tri - Web: 63195048
  // 0.2.2 VnExpress.net - Giai tri - Mobile: 63399995
  // 0.3.1 VnExpress.net - Doi song - Web: 65147722
  // 0.3.2 VnExpress.net - Doi song - Mobile: 65149017
  // 0.4.1 VnExpress.net - Kinh doanh - Web: 70717509
  // 0.4.2 VnExpress.net - Kinh doanh - Mobile: 70715721
  // 0.5.1 VnExpress.net - Du lich - Web: 74009205
  // 0.5.2 VnExpress.net - Du lich - Mobile: 73992885
  // 0.6.1 VnExpress.net - So hoa - Web: 40525692
  // 0.6.2 VnExpress.net - So hoa - Mobile: 22229459
  // 0.6.3 VnExpress.net - So hoa dau gia - Web: 42346779
  // 0.7.1 VnExpress.net - Game thu - Web: 363157
  // 0.7.2 VnExpress.net - Game thu - Forum: 5760529
  // 0.7.3 VnExpress.net - Game thu - Web/forum: 40526634
  // 0.8.1 VnExpress.net - Tim kiem - Web: 58713919

  private static final String TABLE_ID = "ga:40525857";

  private static final int MAX_RESULTS = 10000;

  static DataStoreConfigs configInfo;
  
  static {
    configInfo = DataStoreConfigs.load();
    try {
    } catch (Throwable t) {
        System.out.println(t.getMessage());
    }
}
  
  /** Directory to store user credentials. */
  private static final java.io.File DATA_STORE_DIR = new java.io.File(
      configInfo.path, ".store/analytics_sample");

  /**
   * Global instance of the {@link DataStoreFactory}. The best practice is to make it a single
   * globally shared instance across your application.
   */
  private static FileDataStoreFactory DATA_STORE_FACTORY;

  /** Global instance of the HTTP transport. */
  private static HttpTransport HTTP_TRANSPORT;

  /** Global instance of the JSON factory. */
  private static final JsonFactory JSON_FACTORY = new JacksonFactory();

  private final static Logger LOGGER = Logger.getLogger(AnalyticsApi.class.getName());

  /**
   * Main demo. This first initializes an Analytics service object. It then queries for the top 25
   * organic search keywords and traffic sources by visits. Finally each important part of the
   * response is printed to the screen.
   * 
   * @param args command line args.
   * @throws Exception
   */
  public static void main(String[] args) throws Exception {
    // try {
    // HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
    // DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
    // Analytics analytics = initializeAnalytics();
    // GaData gaData = executeDataQuery3(analytics, TABLE_ID, "2014-06-19", "2014-06-19");
    //
    // printReportInfo(gaData);
    // printProfileInfo(gaData);
    // printQueryInfo(gaData);
    // printPaginationInfo(gaData);
    // printTotalsForAllResults(gaData);
    // printColumnHeaders(gaData);
    // printDataTable(gaData);
    // // printFile2(gaData);
    // // printFile(analytics, gaData, "2");
    //
    // } catch (GoogleJsonResponseException e) {
    // System.err.println("There was a service error: " + e.getDetails().getCode() + " : "
    // + e.getDetails().getMessage());
    // } catch (Throwable t) {
    // t.printStackTrace();
    // }
  }

  /** Authorizes the installed application to access user's protected data. */
  private static Credential authorize() throws Exception {
    // load client secrets
    GoogleClientSecrets clientSecrets =
        GoogleClientSecrets.load(JSON_FACTORY,
            new InputStreamReader(AnalyticsApi.class.getResourceAsStream("/client_secrets.json")));
    if (clientSecrets.getDetails().getClientId().startsWith("Enter")
        || clientSecrets.getDetails().getClientSecret().startsWith("Enter ")) {
      System.out
          .println("Enter Client ID and Secret from https://code.google.com/apis/console/?api=analytics "
              + "into analytics-cmdline-sample/src/main/resources/client_secrets.json");
      System.exit(1);
    }
    // set up authorization code flow
    GoogleAuthorizationCodeFlow flow =
        new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets,
            Collections.singleton(AnalyticsScopes.ANALYTICS_READONLY)).setDataStoreFactory(
            DATA_STORE_FACTORY).build();
    // authorize
    return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
  }

  /**
   * Performs all necessary setup steps for running requests against the API.
   * 
   * @return an initialized Analytics service object.
   * 
   * @throws Exception if an issue occurs with OAuth2Native authorize.
   */
  private static Analytics initializeAnalytics() throws Exception {
    // Authorization.
    Credential credential = authorize();

    // Set up and return Google Analytics API client.
    return new Analytics.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(
        APPLICATION_NAME).build();
  }

  private static GaData executeDataQuery2(Analytics analytics, String tableId, String startDate,
      String endDate) throws IOException {
    return analytics
        .data()
        .ga()
        .get(tableId, // Table Id.
            startDate, // Start date.
            endDate, // End date.
            "ga:pageviews, ga:visitors, ga:sessions, ga:timeOnPage")
        // Metrics.
        .setDimensions(
            "ga:hostname,ga:operatingSystem,ga:browser,ga:deviceCategory,ga:mobileDeviceModel,ga:screenResolution,ga:networkLocation")
        .setSort("-ga:pageviews, ga:visitors, ga:sessions, ga:timeOnPage")
        .setMaxResults(MAX_RESULTS).execute();
  }

  private static GaData executeDataQuery3(Analytics analytics, String tableId, String startDate,
      String endDate) throws IOException {
    return analytics
        .data()
        .ga()
        .get(tableId, // Table Id.
            startDate, // Start date.
            endDate, // End date.
            "ga:pageviews, ga:visitors, ga:newUsers, ga:sessions, ga:timeOnPage")
        // Metrics.
        .setDimensions("ga:hostname,ga:userGender,ga:userAgeBracket,ga:language,ga:city,ga:country")
        .setSort("-ga:pageviews, ga:visitors, ga:newUsers, ga:sessions, ga:timeOnPage")
        .setMaxResults(MAX_RESULTS).execute();
  }

  /**
   * Prints general information about this report.
   * 
   * @param gaData the data returned from the API.
   */
  private static void printReportInfo(GaData gaData) {
    System.out.println();
    System.out.println("Response:");
    System.out.println("ID:" + gaData.getId());
    System.out.println("Self link: " + gaData.getSelfLink());
    System.out.println("Kind: " + gaData.getKind());
    System.out.println("Contains Sampled Data: " + gaData.getContainsSampledData());
  }

  /**
   * Prints general information about the profile from which this report was accessed.
   * 
   * @param gaData the data returned from the API.
   */
  private static void printProfileInfo(GaData gaData) {
    ProfileInfo profileInfo = gaData.getProfileInfo();

    System.out.println("Profile Info");
    System.out.println("Account ID: " + profileInfo.getAccountId());
    System.out.println("Web Property ID: " + profileInfo.getWebPropertyId());
    System.out.println("Internal Web Property ID: " + profileInfo.getInternalWebPropertyId());
    System.out.println("Profile ID: " + profileInfo.getProfileId());
    System.out.println("Profile Name: " + profileInfo.getProfileName());
    System.out.println("Table ID: " + profileInfo.getTableId());
  }

  /**
   * Prints the values of all the parameters that were used to query the API.
   * 
   * @param gaData the data returned from the API.
   */
  private static void printQueryInfo(GaData gaData) {
    Query query = gaData.getQuery();

    System.out.println("Query Info:");
    System.out.println("Ids: " + query.getIds());
    System.out.println("Start Date: " + query.getStartDate());
    System.out.println("End Date: " + query.getEndDate());
    System.out.println("Metrics: " + query.getMetrics()); // List
    System.out.println("Dimensions: " + query.getDimensions()); // List
    System.out.println("Sort: " + query.getSort()); // List
    System.out.println("Segment: " + query.getSegment());
    System.out.println("Filters: " + query.getFilters());
    System.out.println("Start Index: " + query.getStartIndex());
    System.out.println("Max Results: " + query.getMaxResults());
  }

  /**
   * Prints common pagination information.
   * 
   * @param gaData the data returned from the API.
   */
  private static void printPaginationInfo(GaData gaData) {
    System.out.println("Pagination Info:");
    System.out.println("Previous Link: " + gaData.getPreviousLink());
    System.out.println("Next Link: " + gaData.getNextLink());
    System.out.println("Items Per Page: " + gaData.getItemsPerPage());
    System.out.println("Total Results: " + gaData.getTotalResults());
  }

  /**
   * Prints the total metric value for all rows the query matched.
   * 
   * @param gaData the data returned from the API.
   */
  private static void printTotalsForAllResults(GaData gaData) {
    System.out.println("Metric totals over all results:");
    Map<String, String> totalsMap = gaData.getTotalsForAllResults();
    for (Map.Entry<String, String> entry : totalsMap.entrySet()) {
      System.out.println(entry.getKey() + " : " + entry.getValue());
    }
  }

  /**
   * Prints the information for each column. The reporting data from the API is returned as rows of
   * data. The column headers describe the names and types of each column in rows.
   * 
   * @param gaData the data returned from the API.
   */
  private static void printColumnHeaders(GaData gaData) {
    System.out.println("Column Headers:");

    for (ColumnHeaders header : gaData.getColumnHeaders()) {
      System.out.println("Column Name: " + header.getName());
      System.out.println("Column Type: " + header.getColumnType());
      System.out.println("Column Data Type: " + header.getDataType());
    }
  }

  /**
   * Prints all the rows of data returned by the API.
   * 
   * @param gaData the data returned from the API.
   */
  private static void printDataTable(GaData gaData) {
    if (gaData.getTotalResults() > 0) {
      System.out.println("Data Table:");

      // Print the column names.
      for (ColumnHeaders header : gaData.getColumnHeaders()) {
        System.out.format("%-32s", header.getName());
      }
      System.out.println();

      // Print the rows of data.
      for (List<String> rowValues : gaData.getRows()) {
        // System.out.print(rowValues.get(0) + rowValues.get(1) + "\t");
        // for (int i = 2; i < rowValues.size(); i++) {
        // System.out.print(rowValues.get(i) + "\t");
        // }
        for (String value : rowValues) {
          System.out.format("%-32s", value);
        }
        System.out.println();
      }
    } else {
      System.out.println("No data");
    }
  }

  private static GaData executeDataQuery(Analytics analytics, String profileId, String startDate,
      String endDate, String metric, String dimensions, String sort, int startIndex, int maxResults)
      throws IOException {
    return analytics.data().ga().get("ga:" + profileId, // Table Id. ga: + profile id.
        startDate, // Start date.
        endDate, // End date.
        metric)
        // Metrics.
        .setDimensions(dimensions).setSort(sort).setStartIndex(startIndex)
        .setMaxResults(maxResults).execute();
  }

  public static String join(String delimiter, Object... array) {
    StringBuilder s = new StringBuilder();
    int l = array.length, lastIndex = l - 1;
    for (int i = 0; i < l; i++) {
      if (i < lastIndex) {
        s.append(array[i]).append(delimiter);
      } else {
        s.append(array[i]);
      }
    }
    return s.toString();
  }

  public static List<GaDailyTech> getGaDailyTechs(String startDate, String endDate)
      throws Exception {
    LOGGER.setLevel(Level.INFO);
    List<GaDailyTech> gaDailyTechs = new ArrayList<GaDailyTech>();
    HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
    DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
    Analytics analytics = initializeAnalytics();
    GaData gaData = executeDataQuery2(analytics, TABLE_ID, startDate, endDate);
    Query query = gaData.getQuery();
    if (gaData.getRows().size() > 0) {
      for (List<String> rowValues : gaData.getRows()) {
        GaDailyTech gaDailyTech =
            new GaDailyTech(query.getStartDate(), rowValues.get(0), rowValues.get(1),
                rowValues.get(2), rowValues.get(3), rowValues.get(4), rowValues.get(5),
                rowValues.get(6), StringUtil.safeParseInt(rowValues.get(7)),
                StringUtil.safeParseInt(rowValues.get(8)),
                StringUtil.safeParseInt(rowValues.get(9)), StringUtil.safeParseDouble(rowValues
                    .get(10)));
        gaDailyTech = GaDailyTech.convertGaDailyTechDb(gaDailyTech);
        gaDailyTechs.add(gaDailyTech);
      }

      int totalResults = gaData.getTotalResults();
      System.out.println(totalResults);
      ProfileInfo profileInfo = gaData.getProfileInfo();
      List<String> metrics = query.getMetrics();
      StringBuffer metric = new StringBuffer();
      for (int j = 0; j < metrics.size(); j++) {
        if (j > 0) {
          metric.append(",");
        }
        metric.append(metrics.get(j));
      }
      int loop = (totalResults - MAX_RESULTS) / MAX_RESULTS;
      if (loop >= 1) {
        for (int i = 1; i <= loop; i++) {
          GaData ga =
              executeDataQuery(analytics, profileInfo.getProfileId(), query.getStartDate(),
                  query.getEndDate(), metric.toString(), query.getDimensions(),
                  "-" + metric.toString(), MAX_RESULTS * i + 1, MAX_RESULTS);
          for (List<String> rowValues : ga.getRows()) {
            GaDailyTech gaDailyTech =
                new GaDailyTech(query.getStartDate(), rowValues.get(0), rowValues.get(1),
                    rowValues.get(2), rowValues.get(3), rowValues.get(4), rowValues.get(5),
                    rowValues.get(6), StringUtil.safeParseInt(rowValues.get(7)),
                    StringUtil.safeParseInt(rowValues.get(8)), StringUtil.safeParseInt(rowValues
                        .get(9)), StringUtil.safeParseDouble(rowValues.get(10)));
            gaDailyTech = GaDailyTech.convertGaDailyTechDb(gaDailyTech);
            gaDailyTechs.add(gaDailyTech);
          }
        }
      }

      int mod = totalResults % MAX_RESULTS;
      if (mod != 0) {
        GaData ga =
            executeDataQuery(analytics, profileInfo.getProfileId(), query.getStartDate(),
                query.getEndDate(), metric.toString(), query.getDimensions(),
                "-" + metric.toString(), MAX_RESULTS * loop + 1, mod);
        for (List<String> rowValues : ga.getRows()) {
          GaDailyTech gaDailyTech =
              new GaDailyTech(query.getStartDate(), rowValues.get(0), rowValues.get(1),
                  rowValues.get(2), rowValues.get(3), rowValues.get(4), rowValues.get(5),
                  rowValues.get(6), StringUtil.safeParseInt(rowValues.get(7)),
                  StringUtil.safeParseInt(rowValues.get(8)), StringUtil.safeParseInt(rowValues
                      .get(9)), StringUtil.safeParseDouble(rowValues.get(10)));
          gaDailyTech = GaDailyTech.convertGaDailyTechDb(gaDailyTech);
          gaDailyTechs.add(gaDailyTech);
        }
      }
    } else {
      LOGGER.info("executeDataQuery GaDailyTech " + startDate + " null");
    }


    return gaDailyTechs;
  }

  public static List<GaDailyUser> getGaDailyUsers(String startDate, String endDate)
      throws Exception {
    LOGGER.setLevel(Level.INFO);
    List<GaDailyUser> gaDailyUsers = new ArrayList<GaDailyUser>();
    HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
    DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
    Analytics analytics = initializeAnalytics();
    GaData gaData = executeDataQuery3(analytics, TABLE_ID, startDate, endDate);
    Query query = gaData.getQuery();
    if (gaData.getRows().size() > 0) {
      for (List<String> rowValues : gaData.getRows()) {
        int returningVisitor =
            StringUtil.safeParseInt(rowValues.get(7)) - StringUtil.safeParseInt(rowValues.get(8));
        GaDailyUser gaDailyUser =
            new GaDailyUser(query.getStartDate(), rowValues.get(0), rowValues.get(1),
                rowValues.get(2), returningVisitor, rowValues.get(3), rowValues.get(4),
                rowValues.get(5), StringUtil.safeParseInt(rowValues.get(6)),
                StringUtil.safeParseInt(rowValues.get(7)),
                StringUtil.safeParseInt(rowValues.get(9)), StringUtil.safeParseDouble(rowValues
                    .get(10)));
        gaDailyUser = GaDailyUser.convertGaDailyUser(gaDailyUser);
        gaDailyUsers.add(gaDailyUser);
      }

      int totalResults = gaData.getTotalResults();
      System.out.println(totalResults);
      ProfileInfo profileInfo = gaData.getProfileInfo();
      List<String> metrics = query.getMetrics();
      StringBuffer metric = new StringBuffer();
      for (int j = 0; j < metrics.size(); j++) {
        if (j > 0) {
          metric.append(",");
        }
        metric.append(metrics.get(j));
      }
      int loop = (totalResults - MAX_RESULTS) / MAX_RESULTS;
      if (loop >= 1) {
        for (int i = 1; i <= loop; i++) {
          GaData ga =
              executeDataQuery(analytics, profileInfo.getProfileId(), query.getStartDate(),
                  query.getEndDate(), metric.toString(), query.getDimensions(),
                  "-" + metric.toString(), MAX_RESULTS * i + 1, MAX_RESULTS);
          for (List<String> rowValues : ga.getRows()) {
            int returningVisitor =
                StringUtil.safeParseInt(rowValues.get(7))
                    - StringUtil.safeParseInt(rowValues.get(8));
            GaDailyUser gaDailyUser =
                new GaDailyUser(query.getStartDate(), rowValues.get(0), rowValues.get(1),
                    rowValues.get(2), returningVisitor, rowValues.get(3), rowValues.get(4),
                    rowValues.get(5), StringUtil.safeParseInt(rowValues.get(6)),
                    StringUtil.safeParseInt(rowValues.get(7)), StringUtil.safeParseInt(rowValues
                        .get(9)), StringUtil.safeParseDouble(rowValues.get(10)));
            gaDailyUser = GaDailyUser.convertGaDailyUser(gaDailyUser);
            gaDailyUsers.add(gaDailyUser);
          }
        }
      }

      int mod = totalResults % MAX_RESULTS;
      if (mod != 0) {
        GaData ga =
            executeDataQuery(analytics, profileInfo.getProfileId(), query.getStartDate(),
                query.getEndDate(), metric.toString(), query.getDimensions(),
                "-" + metric.toString(), MAX_RESULTS * loop + 1, mod);
        for (List<String> rowValues : ga.getRows()) {
          int returningVisitor =
              StringUtil.safeParseInt(rowValues.get(7)) - StringUtil.safeParseInt(rowValues.get(8));
          GaDailyUser gaDailyUser =
              new GaDailyUser(query.getStartDate(), rowValues.get(0), rowValues.get(1),
                  rowValues.get(2), returningVisitor, rowValues.get(3), rowValues.get(4),
                  rowValues.get(5), StringUtil.safeParseInt(rowValues.get(6)),
                  StringUtil.safeParseInt(rowValues.get(7)), StringUtil.safeParseInt(rowValues
                      .get(9)), StringUtil.safeParseDouble(rowValues.get(10)));
          gaDailyUser = GaDailyUser.convertGaDailyUser(gaDailyUser);
          gaDailyUsers.add(gaDailyUser);
        }
      }
    } else {
      LOGGER.info("executeDataQuery GaDailyUser " + startDate + " null");
    }

    return gaDailyUsers;
  }
}
