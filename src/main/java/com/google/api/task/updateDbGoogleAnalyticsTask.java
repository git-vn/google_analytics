/*
 * Copyright (c) 2011 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.google.api.task;

import com.google.api.services.samples.analytics.cmdline.AnalyticsApi;

import rfx.core.dao.DbGoogleAnalyticsDAO;
import rfx.core.model.GaDailyTech;
import rfx.core.model.GaDailyUser;
import rfx.core.util.DateTimeUtil;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author dungth5
 * 
 */
public class updateDbGoogleAnalyticsTask extends TimerTask {

  private final static int ONCE_PER_DAY = 1000 * 60 * 60 * 24;

  Timer timer;
  int count = 0;
  String startDate;
  String endDate;

  public updateDbGoogleAnalyticsTask() {
  }

  public updateDbGoogleAnalyticsTask(Timer timer, String startDate, String endDate) {
    this.timer = timer;
    this.startDate = startDate;
    this.endDate = endDate;
  }

  public void toDo(String startDate, String endDate) {

    try {
      DbGoogleAnalyticsDAO dbGoogleAnalyticsDAO = DbGoogleAnalyticsDAO.getInstance();
      List<GaDailyTech> gaDailyTechs = AnalyticsApi.getGaDailyTechs(startDate, endDate);
      dbGoogleAnalyticsDAO.updateGaDailyTech(gaDailyTechs);
      List<GaDailyUser> gaDailyUsers = AnalyticsApi.getGaDailyUsers(startDate, endDate);
      dbGoogleAnalyticsDAO.updateGaDailyUser(gaDailyUsers);

    } catch (Exception exception) {
      exception.printStackTrace();
    }
  }

  @Override
  public void run() {
    toDo(startDate, endDate);
    System.out.println("updateDbGoogleAnalyticsTask finish!!!(-_-)");
    // timer.cancel();
  }

  public static void main(String args[]) {
    Timer timer = new Timer();
    updateDbGoogleAnalyticsTask myTask = new updateDbGoogleAnalyticsTask(timer, args[0], args[1]);
    int firstSart = 1000; // it means after 1 second.
    int period = ONCE_PER_DAY; // after which the task
    // will repeat;
    timer.schedule(myTask, firstSart, period);// the time
    // specified in millisecond.
  }
}
