/*
 * Copyright (c) 2011 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package rfx.core.model;

/**
 * @author dungth5
 * 
 */

public class GaDailyTech {
  private String date;
  private String site = "-";
  private String os = "-";
  private String browser = "-";
  private String deviceType = "-";
  private String deviceName = "-";
  private String resolution = "-";
  private String isp = "-";
  private int pageview = -1;
  private int visitAmount = -1;
  private int visitorAmount = -1;
  private double time = -1;

  public GaDailyTech() {

  }


  public GaDailyTech(String date, String site, String os, String browser, String deviceType,
      String deviceName, String resolution, String isp, int pageview, int visitAmount,
      int visitorAmount, double time) {
    super();
    this.date = date;
    this.site = site;
    this.os = os;
    this.browser = browser;
    this.deviceType = deviceType;
    this.deviceName = deviceName;
    this.resolution = resolution;
    this.isp = isp;
    this.pageview = pageview;
    this.visitAmount = visitAmount;
    this.visitorAmount = visitorAmount;
    this.time = time;
  }


  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public String getSite() {
    return site;
  }

  public void setSite(String site) {
    this.site = site;
  }

  public String getOs() {
    return os;
  }

  public void setOs(String os) {
    this.os = os;
  }

  public String getBrowser() {
    return browser;
  }

  public void setBrowser(String browser) {
    this.browser = browser;
  }

  public String getDeviceType() {
    return deviceType;
  }

  public void setDeviceType(String deviceType) {
    this.deviceType = deviceType;
  }

  public String getDeviceName() {
    return deviceName;
  }

  public void setDeviceName(String deviceName) {
    this.deviceName = deviceName;
  }

  public String getResolution() {
    return resolution;
  }

  public void setResolution(String resolution) {
    this.resolution = resolution;
  }

  public String getIsp() {
    return isp;
  }

  public void setIsp(String isp) {
    this.isp = isp;
  }

  public int getPageview() {
    return pageview;
  }

  public void setPageview(int pageview) {
    this.pageview = pageview;
  }

  public int getVisitAmount() {
    return visitAmount;
  }

  public void setVisitAmount(int visitAmount) {
    this.visitAmount = visitAmount;
  }

  public int getVisitorAmount() {
    return visitorAmount;
  }

  public void setVisitorAmount(int visitorAmount) {
    this.visitorAmount = visitorAmount;
  }

  public double getTime() {
    return time;
  }

  public void setTime(double time) {
    this.time = time;
  }
  
  public static GaDailyTech convertGaDailyTechDb(GaDailyTech ga) {
    ga.setSite("(not set)".equals(ga.getSite()) ? "-" : ga.getSite());
    ga.setOs("(not set)".equals(ga.getOs()) ? "-" : ga.getOs());
    ga.setBrowser("(not set)".equals(ga.getBrowser()) ? "-" : ga.getBrowser());
    ga.setDeviceType("(not set)".equals(ga.getDeviceType()) ? "-" : ga.getDeviceType());
    ga.setDeviceName("(not set)".equals(ga.getDeviceName()) ? "-" : ga.getDeviceName());
    ga.setResolution("(not set)".equals(ga.getResolution()) ? "-" : ga.getResolution());
    ga.setIsp("(not set)".equals(ga.getIsp()) ? "-" : ga.getIsp());
    return ga;
  }
}
