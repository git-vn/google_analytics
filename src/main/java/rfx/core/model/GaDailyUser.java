/*
 * Copyright (c) 2011 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package rfx.core.model;

/**
 * @author dungth5
 * 
 */
public class GaDailyUser {
  private String date;
  private String site = "-";
  private String gender = "-";
  private String age = "-";
  private int visitorReturning = -1;
  private String language = "-";
  private String province = "-";
  private String country = "-";
  private int pageview = -1;
  private int visitAmount = -1;
  private int visitorAmount = -1;
  private double time = -1;

  public GaDailyUser() {

  }

  public GaDailyUser(String date, String site, String gender, String age, int visitorReturning,
      String language, String province, String country, int pageview, int visitAmount,
      int visitorAmount, double time) {
    super();
    this.date = date;
    this.site = site;
    this.gender = gender;
    this.age = age;
    this.visitorReturning = visitorReturning;
    this.language = language;
    this.province = province;
    this.country = country;
    this.pageview = pageview;
    this.visitAmount = visitAmount;
    this.visitorAmount = visitorAmount;
    this.time = time;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public String getSite() {
    return site;
  }

  public void setSite(String site) {
    this.site = site;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getAge() {
    return age;
  }

  public void setAge(String age) {
    this.age = age;
  }

  public int getVisitorReturning() {
    return visitorReturning;
  }

  public void setVisitorReturning(int visitorReturning) {
    this.visitorReturning = visitorReturning;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public int getPageview() {
    return pageview;
  }

  public void setPageview(int pageview) {
    this.pageview = pageview;
  }

  public int getVisitAmount() {
    return visitAmount;
  }

  public void setVisitAmount(int visitAmount) {
    this.visitAmount = visitAmount;
  }

  public int getVisitorAmount() {
    return visitorAmount;
  }

  public void setVisitorAmount(int visitorAmount) {
    this.visitorAmount = visitorAmount;
  }

  public double getTime() {
    return time;
  }

  public void setTime(double time) {
    this.time = time;
  }
  
  public static GaDailyUser convertGaDailyUser(GaDailyUser ga) {
    ga.setSite("(not set)".equals(ga.getSite()) ? "-" : ga.getSite());
    ga.setGender("(not set)".equals(ga.getGender()) ? "-" : ga.getGender());
    ga.setAge("(not set)".equals(ga.getAge()) ? "-" : ga.getAge());
    ga.setLanguage("(not set)".equals(ga.getLanguage()) ? "-" : ga.getLanguage());
    ga.setProvince("(not set)".equals(ga.getProvince()) ? "-" : ga.getProvince());
    ga.setCountry("(not set)".equals(ga.getCountry()) ? "-" : ga.getCountry());
    return ga;
  }
}
