package rfx.core.dao;
import java.sql.Connection;

import javax.sql.DataSource;
import javax.sql.PooledConnection;

import oracle.jdbc.pool.OracleConnectionPoolDataSource;

import org.apache.commons.dbcp.BasicDataSource;

public class OracleConnManager {
	static DataSource oracleDataSource;
	static DataSource oracleDataSourceReal;

	static OracleConnectionPoolDataSource ocpds;
	static PooledConnection connectionPool;
	public static final String TIMEOUT_IDLE_MESSAGE = "Timeout";

	public static synchronized Connection getConnection() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			if (oracleDataSource == null) {
				oracleDataSource = getOracleDataSource();
				if (oracleDataSource != null) {
					return oracleDataSource.getConnection();
				}
				return null;
			}
			return oracleDataSource.getConnection();
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	private static synchronized DataSource getOracleDataSource() {

		String connectionUrl = "jdbc:oracle:thin:@" + DbGenericDao.getSqlDbGoogleAnalyticsConfigs().getHost() + ":" + DbGenericDao.getSqlDbGoogleAnalyticsConfigs().getPort() + "/"
				+ DbGenericDao.getSqlDbGoogleAnalyticsConfigs().getDatabase();

//		DriverManager.setLoginTimeout(ConfigInfo.ORACLE_TIMEOUT);
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(DbGenericDao.getSqlDbGoogleAnalyticsConfigs().getDbdriverclasspath());
		dataSource.setUsername(DbGenericDao.getSqlDbGoogleAnalyticsConfigs().getUsername());
		dataSource.setPassword(DbGenericDao.getSqlDbGoogleAnalyticsConfigs().getPassword());
		dataSource.setUrl(connectionUrl);

		dataSource.setMaxActive(-1);
		dataSource.setMinIdle(-1);
		dataSource.setMaxWait(15000);
		dataSource.setDefaultAutoCommit(true);
		dataSource.setAccessToUnderlyingConnectionAllowed(true);
		// System.out.println(dataSource.getMaxActive());
		return dataSource;
	}
}
