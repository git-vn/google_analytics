package rfx.core.dao;

import com.google.gson.Gson;

import rfx.core.model.GaDailyTech;
import rfx.core.model.GaDailyUser;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DbGoogleAnalyticsDAO extends DbGenericDao {
  private final static Logger LOGGER = Logger.getLogger(DbGoogleAnalyticsDAO.class.getName());

  final static String SP_UPD_FACT_GA_DAILY_TECH =
      "{call SP_UPD_FACT_GA_DAILY_TECH (?,?,?,?,?,?,?,?,?,?,?,?)}";
  final static String SP_UPD_FACT_GA_DAILY_USER =
      "{call SP_UPD_FACT_GA_DAILY_USER (?,?,?,?,?,?,?,?,?,?,?,?)}";

  static DbGoogleAnalyticsDAO instance = new DbGoogleAnalyticsDAO();

  public static final DbGoogleAnalyticsDAO getInstance() {
    return instance;
  }

  protected DbGoogleAnalyticsDAO() {
  }

  final public boolean updateGaDailyTech(final List<GaDailyTech> gaDailyTechs) {
    LOGGER.setLevel(Level.INFO);
    PreparedStatement ps = null;
    // OracleConnection con = null;
    Connection con = null;
    boolean ok = false;
    try {
      con = OracleConnManager.getConnection();
      org.apache.commons.dbcp.DelegatingConnection del =
          new org.apache.commons.dbcp.DelegatingConnection(OracleConnManager.getConnection());
      con = del.getInnermostDelegate();

      ps = con.prepareCall(SP_UPD_FACT_GA_DAILY_TECH);
      long pageview = 0;
      for (GaDailyTech ga : gaDailyTechs) {
        pageview += ga.getPageview();
        ps.setString(1, ga.getDate());
        ps.setString(2, ga.getSite());
        ps.setString(3, ga.getOs());
        ps.setString(4, ga.getBrowser());
        ps.setString(5, ga.getDeviceType());
        ps.setString(6, ga.getDeviceName());
        ps.setString(7, ga.getResolution());
        ps.setString(8, ga.getIsp());
        ps.setInt(9, ga.getPageview());
        ps.setInt(10, ga.getVisitAmount());
        ps.setInt(11, ga.getVisitorAmount());
        ps.setDouble(12, ga.getTime());
        LOGGER.info("DbGoogleAnalyticsDAO.updateGaDailyTech:" + new Gson().toJson(ga));
        // System.out.println("DbGoogleAnalyticsDAO.updateGaDailyTech:" + new Gson().toJson(ga));
        ps.addBatch();
      }
      System.out.println(pageview);
      ps.executeBatch();
    } catch (Throwable exception) {
      // System.err.println("save fail with params: " + ga);
      DbGenericDao.dbExceptionHandler(exception);
    } finally {
      if (ps != null) {
        try {
          ps.close();
        } catch (SQLException e1) {
        }
      }
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e1) {
        }
      }
    }
    return ok;
  }

  final public boolean updateGaDailyUser(final List<GaDailyUser> gaDailyUsers) {
    LOGGER.setLevel(Level.INFO);
    PreparedStatement ps = null;
    // OracleConnection con = null;
    Connection con = null;
    boolean ok = false;
    try {
      con = OracleConnManager.getConnection();
      org.apache.commons.dbcp.DelegatingConnection del =
          new org.apache.commons.dbcp.DelegatingConnection(OracleConnManager.getConnection());
      con = del.getInnermostDelegate();

      ps = con.prepareCall(SP_UPD_FACT_GA_DAILY_USER);
      long pageview = 0;
      for (GaDailyUser ga : gaDailyUsers) {
        pageview += ga.getPageview();
        ps.setString(1, ga.getDate());
        ps.setString(2, ga.getSite());
        ps.setString(3, ga.getGender());
        ps.setString(4, ga.getAge());
        ps.setInt(5, ga.getVisitorReturning());
        ps.setString(6, ga.getLanguage());
        ps.setString(7, ga.getProvince());
        ps.setString(8, ga.getCountry());
        ps.setInt(9, ga.getPageview());
        ps.setInt(10, ga.getVisitAmount());
        ps.setInt(11, ga.getVisitorAmount());
        ps.setDouble(12, ga.getTime());
        LOGGER.info("DbGoogleAnalyticsDAO.updateGaDailyUser:" + new Gson().toJson(ga));
        // System.out.println("DbGoogleAnalyticsDAO.updateGaDailyUser:" + new Gson().toJson(ga));
        ps.addBatch();
      }
      System.out.println(pageview);
      ps.executeBatch();
    } catch (Throwable exception) {
      // System.err.println("save fail with params: " + ga);
      DbGenericDao.dbExceptionHandler(exception);
    } finally {
      if (ps != null) {
        try {
          ps.close();
        } catch (SQLException e1) {
        }
      }
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e1) {
        }
      }
    }
    return ok;
  }

  public static void main(String[] args) {
  }
}
